# igore this... old af

# Prerequisites

1. npm
2. ruby

# Install neovim

```
bash $(curl -fsSL https://raw.githubusercontent.com/tomas-kucharik/dotfiles/master/setup-nvim.sh)
```

# Install zsh and OhMyZsh

```
bash $(curl -fsSL https://raw.githubusercontent.com/tomas-kucharik/dotfiles/master/setup-ohmyzsh.sh)
```
